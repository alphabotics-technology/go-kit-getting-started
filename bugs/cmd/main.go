package main

import service "go-kit-getting-started/bugs/cmd/service"

func main() {
	service.Run()
}
