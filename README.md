# Getting started with Go-kit

## Planning

We will design a Microservices application with functions as follows:

- Users should have REST API endpoint for creation, update, delete.
- Notificator should not have REST API as it's an internal service

## Getting started

Step 1: Firstly, you need to install go-kit CLI and the project must be in the $GOPATH/src folder for the generator to work

```shell
cd /home/$USER/go/src
mkdir go-kit-getting-started
go get github.com/go-kit/kit
go get github.com/kujtimiihoxha/kit
```

Step 2: Create our services

```shell
kit new service users
kit new service bugs
kit new service notificator
```

This will generate the initial folder structure and the empty service interface by default.

Step 3: Define some functions in the interface.

Now, we need to create some methods for:

- User creation.
- Bug creattion.
- SendEmail (Notificator).

#### Users, Bugs service
Open service.go in the services directories and add these codes:

users/service.go

```
type UsersService interface {
    // Add your methods here
    Create(ctx context.Context, email string) error
}
```

bugs/service.go

```
type BugsService interface {
    Create(ctx context.Context, bug string) error
}
```

Then, we need to run command to generate default endpoint middleware, logging middleware with option --dmw .

```shell
kit generate service users --dmw
kit generate service bugs --dmw
```

This will create a folder named "cmd/" includes service endpoints and logging.

#### Notificator service

Notificator should not have REST API as it's an internal service, so we generate service with gRPC transport. gRPC stands for Google RPC framework, you can find here: https://grpc.io.

1. Update nificator service file as follows: 

    notificator/service.go

    ```
    type NotificatorService interface {
        SendEmail(ctx context.Context, email string, content string) error
    }
    ```

2. Generate this service with gRPC transport

    Besure you have installed **protoc** and **protoc-gen-go** library to use gRPC.

    ```
    go get -u github.com/golang/protobuf/protoc-gen-go
    ```

    Install [protoc and protobuf](https://developers.google.com/protocol-buffers/docs/gotutorial):

    1. From [this page](https://github.com/protocolbuffers/protobuf/releases/tag/v3.6.1), download the protobuf-all-[VERSION].tar.gz

        ```
        curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.6.1/protobuf-all-3.6.1.tar.gz
        ```

    2. Extract the contents and change in the directory

        ```
        tar xvzf protobuf-all-3.6.1.tar.gz -C protoc3
        cd protoc3
        cd protobuf-all-3.6.1
        ```
    4. ./configure
    5. make
    6. make check
    7. sudo make install
    8. sudo ldconfig # refresh shared library cache.
    9. protoc --version


Generate Notificator as gRPC service with this command:

```
kit generate service notificator -t grpc --dmw
```

### Docker support

go-kit CLI can also create a boilerplate docker-compose setup, let's try it.

```
kit generate docker
```


### Reference & Credit

- [Microservices with go-kit article by Alex Pliutau](https://dev.to/plutov/microservices-with-go-kit-part-1-13dd)