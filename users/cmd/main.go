package main

import service "go-kit-getting-started/users/cmd/service"

func main() {
	service.Run()
}
