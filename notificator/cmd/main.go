package main

import service "go-kit-getting-started/notificator/cmd/service"

func main() {
	service.Run()
}
